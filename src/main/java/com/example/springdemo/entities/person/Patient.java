package com.example.springdemo.entities.person;

import com.example.springdemo.entities.enums.Gender;
import com.example.springdemo.entities.enums.Role;
import com.example.springdemo.entities.medication.MedicationPlan;

import javax.persistence.*;

@Entity
@Table(name = "PATIENT")
public class Patient extends Person{


    @ManyToOne
    @JoinColumn(name="caregiver_id")
    private Caregiver careGiver;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "medication_plan_id", referencedColumnName = "id")
    private MedicationPlan medicationPlan;

    public Patient() {
    }

    public Patient(Integer id, String name,  Caregiver careGiver, MedicationPlan medicationPlan) {
        super(id, name, Role.PATIENT);
        this.careGiver = careGiver;
        this.medicationPlan = medicationPlan;
    }

    public Patient(Integer id, String name, String birthdate, Gender gender, String username, String password, Caregiver careGiver, MedicationPlan medicationPlan) {
        super(id, name, birthdate, gender, Role.PATIENT, username, password);
        this.careGiver = careGiver;
        this.medicationPlan = medicationPlan;
    }

    public Caregiver getCareGiver() {
        return careGiver;
    }

    public void setCareGiver(Caregiver careGiver) {
        this.careGiver = careGiver;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

}
