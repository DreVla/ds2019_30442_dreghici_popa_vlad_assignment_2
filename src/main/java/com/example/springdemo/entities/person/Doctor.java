package com.example.springdemo.entities.person;

import com.example.springdemo.entities.enums.Gender;
import com.example.springdemo.entities.enums.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "DOCTOR")
public class Doctor extends Person {

    @Column(name = "medical_record")
    private String medicalRecord;

    public Doctor() {
    }

    public Doctor(Integer id, String name, String medicalRecord) {
        super(id, name, Role.DOCTOR);
        this.medicalRecord = medicalRecord;
    }

    public Doctor(Integer id, String name, String birthdate, Gender gender, String username, String password, String medicalRecord) {
        super(id, name, birthdate, gender, Role.DOCTOR, username, password);
        this.medicalRecord = medicalRecord;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}