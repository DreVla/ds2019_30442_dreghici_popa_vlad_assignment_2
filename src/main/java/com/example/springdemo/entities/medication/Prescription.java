package com.example.springdemo.entities.medication;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.util.Arrays;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "PRESCRIPTION")
public class Prescription {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "drug_id", referencedColumnName = "id")
    private Drug drug;

    @Column(name = "intake")
    private String intake;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="medication_plan_id", nullable=false)
    private MedicationPlan medicationPlan;

    public Prescription() {
    }

    public Prescription(Integer id, Drug drug, String intake, MedicationPlan medicationPlan) {
        this.id = id;
        this.drug = drug;
        this.intake = intake;
        this.medicationPlan = medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    @Override
    public String toString() {
        return "Prescription{" +
                "id=" + id +
                ", drug=" + drug +
                ", intake='" + intake + '\'' +
                ", medicationPlan=" + medicationPlan +
                '}';
    }
}
