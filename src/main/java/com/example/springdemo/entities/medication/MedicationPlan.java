package com.example.springdemo.entities.medication;


import com.example.springdemo.entities.person.Patient;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "MEDICATION_PLAN")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;


    @OneToMany(mappedBy="medicationPlan")
    private Set<Prescription> prescriptions;

    @JsonIgnore
    @OneToOne(mappedBy = "medicationPlan")
    private Patient patient;

    public MedicationPlan() {
    }

    public MedicationPlan(Integer id, Set<Prescription> prescription, Patient patient) {
        this.id = id;
        this.prescriptions = prescription;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Set<Prescription> prescription) {
        this.prescriptions = prescription;
    }

    public void addPrescription(Prescription prescription){
        this.prescriptions.add(prescription);
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
