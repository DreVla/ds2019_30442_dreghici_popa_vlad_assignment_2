package com.example.springdemo.controller.activity;

import com.example.springdemo.dto.activity.ActivityDTO;
import com.example.springdemo.dto.activity.ActivityViewDTO;
import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.services.activity.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/activities")
public class ActivityController {

    @Autowired
    private final ActivityService activityService;

    public ActivityController(ActivityService activityService) {
        this.activityService = activityService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<ActivityViewDTO> findAllActivities() {
        return activityService.findAll();
    }


    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Integer insertActivity(@RequestBody ActivityDTO activityDTO){
        return activityService.insert(activityDTO);
    }

}
