package com.example.springdemo.controller.person;

import com.example.springdemo.dto.person.CaregiverDTO;
import com.example.springdemo.dto.person.CaregiverViewDTO;
import com.example.springdemo.dto.person.PatientViewDTO;
import com.example.springdemo.services.person.CaregiverService;
import com.example.springdemo.services.person.PatientService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregivers")
public class CaregiverController {

    private final CaregiverService caregiverService;
    private final PatientService patientService;

    public CaregiverController(CaregiverService caregiverService, PatientService patientService) {
        this.caregiverService = caregiverService;
        this.patientService = patientService;
    }

    /*
    Find doctor by id and get all
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public CaregiverViewDTO findById(@PathVariable("id") Integer id){
        return caregiverService.findUserById(id);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<CaregiverViewDTO> findAllCaregivers(){
        return caregiverService.findAll();
    }

    @RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
    public CaregiverDTO getCaregiverByUsername(@PathVariable("username") String username){
        return caregiverService.getCaregiverByUsername(username);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
        return caregiverService.insert(caregiverDTO);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public void updateCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
        caregiverService.update(caregiverDTO);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void deleteCaregiverDTO(@RequestBody CaregiverViewDTO caregiverViewDTO){
        caregiverService.delete(caregiverViewDTO);
    }


    /*
    Find all patients for the givent caregiver
     */

    @RequestMapping(value = "/{id}/all_patients", method = RequestMethod.GET)
    public List<PatientViewDTO> findAllPatients(@PathVariable("id") Integer id){
        return patientService.findAllForCaregiver(id);
    }

}
