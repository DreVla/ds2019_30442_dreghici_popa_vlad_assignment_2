package com.example.springdemo.controller.person;

import com.example.springdemo.dto.person.CaregiverViewDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.dto.person.PatientViewDTO;
import com.example.springdemo.services.person.CaregiverService;
import com.example.springdemo.services.person.PatientService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/patients")
public class PatientController {


    private final PatientService patientService;

    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }
    /*
    Find patients by id and get all
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PatientViewDTO findById(@PathVariable("id") Integer id){
        return patientService.findUserById(id);
    }


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<PatientViewDTO> findAllPatients(){
        return patientService.findAll();
    }

    @RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
    public PatientDTO getPatientByUsername(@PathVariable("username") String username){
        return patientService.getPatientByUsername(username);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public void updatePatientDTO(@RequestBody PatientDTO patientDTO){
        patientService.update(patientDTO);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public void deletePatientDTO(@RequestBody PatientViewDTO patientViewDTO){
        patientService.delete(patientViewDTO);
    }

}
