package com.example.springdemo.controller.medication;


import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.dto.medication.DrugViewDTO;
import com.example.springdemo.dto.medication.PrescriptionDTO;
import com.example.springdemo.dto.medication.PrescriptionViewDTO;
import com.example.springdemo.services.medication.PrescriptionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/prescriptions")
public class PrescriptionController {

    private PrescriptionService prescriptionService;

    public PrescriptionController(PrescriptionService prescriptionService) {
        this.prescriptionService = prescriptionService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<PrescriptionViewDTO> findAllPrescriptions(){
        return prescriptionService.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PrescriptionViewDTO findPrescriptionById(@PathVariable("id") Integer id){
        return prescriptionService.findUserById(id);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public Integer insertPrescription(@RequestBody PrescriptionDTO prescriptionDTO){
        return prescriptionService.insert(prescriptionDTO);
    }
}
