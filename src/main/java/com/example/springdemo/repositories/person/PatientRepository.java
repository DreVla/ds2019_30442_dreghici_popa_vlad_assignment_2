package com.example.springdemo.repositories.person;

import com.example.springdemo.entities.person.Caregiver;
import com.example.springdemo.entities.person.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

    @Query(value = "SELECT u " + "FROM Patient u " + "ORDER BY u.name")
    List<Patient> getAllOrdered();

    @Query(value = "SELECT p FROM Patient p WHERE p.careGiver = :caregiver")
    List<Patient> getAllPatientsForCaregiver(@Param("caregiver")Caregiver caregiver);

    @Query(value = "SELECT p FROM Patient p WHERE p.username = :username")
    Patient getPatientByUsername(@Param("username")String username);
}
