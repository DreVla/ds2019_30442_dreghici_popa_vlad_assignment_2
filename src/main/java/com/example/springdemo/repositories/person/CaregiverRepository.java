package com.example.springdemo.repositories.person;

import com.example.springdemo.entities.person.Caregiver;
import com.example.springdemo.entities.person.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {

    @Query(value = "SELECT u " + "FROM Caregiver u " + "ORDER BY u.name")
    List<Caregiver> getAllOrdered();

    @Query(value = "SELECT u FROM Caregiver u WHERE u.username = :username")
    Caregiver getCaregiverByUsername(@Param("username")String username);
}
