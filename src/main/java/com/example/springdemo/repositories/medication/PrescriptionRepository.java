package com.example.springdemo.repositories.medication;

import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.entities.medication.Prescription;
import com.example.springdemo.entities.person.Caregiver;
import com.example.springdemo.entities.person.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrescriptionRepository extends JpaRepository<Prescription, Integer> {

    @Query(value = "SELECT u " + "FROM Prescription u " + "ORDER BY u.id")
    List<Prescription> getAllOrdered();

    @Query(value = "SELECT p FROM Prescription p WHERE p.medicationPlan = :medicationplan")
    List<Prescription> getAllPrescriptionsForMedicationPlan(@Param("medicationplan")MedicationPlan medicationPlan);
}