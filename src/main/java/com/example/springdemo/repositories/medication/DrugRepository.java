package com.example.springdemo.repositories.medication;

import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.person.Doctor;
import com.example.springdemo.entities.person.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DrugRepository extends JpaRepository<Drug, Integer> {

    @Query(value = "SELECT u " + "FROM Drug u " + "ORDER BY u.name")
    List<Drug> getAllOrdered();
}
