package com.example.springdemo.amqp;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BrokerConfiguartionApplication {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext("rabbitConfiguration.xml");
        AmqpAdmin amqpAdmin = context.getBean(AmqpAdmin.class);
        Queue helloWorldQueue = new Queue("activity.queue");

        amqpAdmin.declareQueue(helloWorldQueue);

    }
}
