/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.springdemo.amqp;

import com.example.springdemo.controller.activity.ActivityController;
import com.example.springdemo.dto.activity.ActivityDTO;
import com.example.springdemo.entities.activity.Activity;
import com.example.springdemo.repositories.activity.ActivityRepository;
import com.example.springdemo.services.activity.ActivityService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


@RabbitListener(queues = "activity.queue")
public class Tut1Receiver {

	@Autowired
	private ActivityController activityController;

	@RabbitHandler
	public void receive(String in) {
		String startDateString = in.substring(0, 19);
		String endDateString = in.substring(21, 40);
		String activity = in.substring(42);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(startDateString);
			endDate = new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(endDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long durationInMillis = Math.abs(endDate.getTime() - startDate.getTime());
		long durationInMinutes = TimeUnit.MINUTES.convert(durationInMillis,TimeUnit.MILLISECONDS);
		long durationInHours = durationInMinutes/60;
		long leftMinutes = durationInMinutes%60;
		ActivityDTO receivedActivity = new ActivityDTO();
		receivedActivity.setActivity(activity);
		receivedActivity.setStartDate(startDate);
		receivedActivity.setEndDate(endDate);
		receivedActivity.setDuration(durationInHours);
		activityController.insertActivity(receivedActivity);
		System.out.println(startDate + " " + endDate + " " + activity + " " + durationInHours + ":" + leftMinutes);
		if(durationInHours >= 12){
			if(activity.contains("Sleeping") || activity.contains("Leaving")){
				System.out.println("Something is wrong with patient. Check Now!");
			}
		}
		if(durationInHours >=1){
			if(activity.contains("Toileting") || activity.contains("Showering") || activity.contains("Grooming")){
				System.out.println("Something is wrong with patient. Check Now!");
			}
		}
	}

}
