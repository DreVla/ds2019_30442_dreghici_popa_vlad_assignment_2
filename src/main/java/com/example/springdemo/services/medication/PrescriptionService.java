package com.example.springdemo.services.medication;

import com.example.springdemo.dto.builders.medication.PrescriptionBuilder;
import com.example.springdemo.dto.builders.medication.PrescriptionViewBuilder;
import com.example.springdemo.dto.builders.person.PatientViewBuilder;
import com.example.springdemo.dto.medication.PrescriptionDTO;
import com.example.springdemo.dto.medication.PrescriptionViewDTO;
import com.example.springdemo.dto.person.PatientViewDTO;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.entities.medication.Prescription;
import com.example.springdemo.entities.person.Caregiver;
import com.example.springdemo.entities.person.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.medication.MedicationPlanRepository;
import com.example.springdemo.repositories.medication.PrescriptionRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PrescriptionService {

    private final PrescriptionRepository prescriptionRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public PrescriptionService(PrescriptionRepository prescriptionRepository, MedicationPlanRepository medicationPlanRepository) {
        this.prescriptionRepository = prescriptionRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public PrescriptionViewDTO findUserById(Integer id) {
        Optional<Prescription> prescription = prescriptionRepository.findById(id);

        if (!prescription.isPresent()) {
            throw new ResourceNotFoundException("Prescription", "user id", id);
        }
        return PrescriptionViewBuilder.generateDTOFromEntity(prescription.get());
    }

    public List<PrescriptionViewDTO> findAll() {
        List<Prescription> prescriptions = prescriptionRepository.getAllOrdered();

        return prescriptions.stream()
                .map(PrescriptionViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(PrescriptionDTO prescriptionDTO) {

        PersonFieldValidator.validateInsertOrUpdatePrescription(prescriptionDTO);

        return prescriptionRepository
                .save(PrescriptionBuilder.generateEntityFromDTO(prescriptionDTO))
                .getId();
    }

    public Integer update(PrescriptionDTO prescriptionDTO) {

        Optional<Prescription> prescription = prescriptionRepository.findById(prescriptionDTO.getId());

        if (!prescription.isPresent()) {
            throw new ResourceNotFoundException("Person", "user id", prescriptionDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdatePrescription(prescriptionDTO);

        return prescriptionRepository.save(PrescriptionBuilder.generateEntityFromDTO(prescriptionDTO)).getId();
    }

    public void delete(PrescriptionViewDTO prescriptionViewDTO) {
        this.prescriptionRepository.deleteById(prescriptionViewDTO.getId());
    }

    public List<PrescriptionViewDTO> findAllForMedicationPlan(Integer prescription_id) {
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(prescription_id);
        if(medicationPlan.isPresent()){
            List<Prescription> prescriptions = prescriptionRepository.getAllPrescriptionsForMedicationPlan(medicationPlan.get());
            return prescriptions.stream()
                    .map(PrescriptionViewBuilder::generateDTOFromEntity)
                    .collect(Collectors.toList());
        }
        else return null;
    }

}
