package com.example.springdemo.services.medication;

import com.example.springdemo.dto.builders.medication.MedicationPlanBuilder;
import com.example.springdemo.dto.builders.medication.MedicationPlanViewBuilder;
import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.dto.medication.MedicationPlanViewDTO;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.medication.MedicationPlanRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public MedicationPlanViewDTO findUserById(Integer id) {
        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(id);

        if (!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("MedicationPlan", "user id", id);
        }
        return MedicationPlanViewBuilder.generateDTOFromEntity(medicationPlan.get());
    }

    public List<MedicationPlanViewDTO> findAll() {
        List<MedicationPlan> medicationPlans = medicationPlanRepository.getAllOrdered();

        return medicationPlans.stream()
                .map(MedicationPlanViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(MedicationPlanDTO medicationPlanDTO) {

        PersonFieldValidator.validateInsertOrUpdateMedicationPlan(medicationPlanDTO);

        return medicationPlanRepository
                .save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO))
                .getId();
    }

    public Integer update(MedicationPlanDTO medicationPlanDTO) {

        Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(medicationPlanDTO.getId());

        if (!medicationPlan.isPresent()) {
            throw new ResourceNotFoundException("Person", "user id", medicationPlanDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdateMedicationPlan(medicationPlanDTO);

        return medicationPlanRepository.save(MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)).getId();
    }

    public void delete(MedicationPlanViewDTO medicationPlanViewDTO) {
        this.medicationPlanRepository.deleteById(medicationPlanViewDTO.getId());
    }

}
