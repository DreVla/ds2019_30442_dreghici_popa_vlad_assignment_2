package com.example.springdemo.services.person;

import com.example.springdemo.dto.builders.person.PatientBuilder;
import com.example.springdemo.dto.builders.person.PatientViewBuilder;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.dto.person.PatientViewDTO;
import com.example.springdemo.entities.person.Caregiver;
import com.example.springdemo.entities.person.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.person.CaregiverRepository;
import com.example.springdemo.repositories.person.PatientRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
    }

    public PatientViewDTO findUserById(Integer id) {
        Optional<Patient> patient = patientRepository.findById(id);

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", id);
        }
        return PatientViewBuilder.generateDTOFromEntity(patient.get());
    }

    public List<PatientViewDTO> findAll() {
        List<Patient> patients = patientRepository.getAllOrdered();

        return patients.stream()
                .map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<PatientViewDTO> findAllForCaregiver(Integer caregiver_id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiver_id);
        if(caregiver.isPresent()){
            List<Patient> patients = patientRepository.getAllPatientsForCaregiver(caregiver.get());
            return patients.stream()
                    .map(PatientViewBuilder::generateDTOFromEntity)
                    .collect(Collectors.toList());
        }
        else return null;
    }

    public Integer insert(PatientDTO patientDTO) {

        PersonFieldValidator.validateInsertOrUpdatePatient(patientDTO);

        return patientRepository
                .save(PatientBuilder.generateEntityFromDTO(patientDTO))
                .getId();
    }

    public Integer update(PatientDTO patientDTO) {

        Optional<Patient> patient = patientRepository.findById(patientDTO.getId());

        if (!patient.isPresent()) {
            throw new ResourceNotFoundException("Patient", "user id", patientDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdatePatient(patientDTO);

        return patientRepository.save(PatientBuilder.generateEntityFromDTO(patientDTO)).getId();
    }

    public void delete(PatientViewDTO patientViewDTO) {
        this.patientRepository.deleteById(patientViewDTO.getId());
    }

    public PatientDTO getPatientByUsername(String username){
        Patient patientFound = patientRepository.getPatientByUsername(username);
        if(patientFound != null)
            return PatientViewBuilder.generateDTOwithPass(patientFound);
        else
            return null;
    }
}
