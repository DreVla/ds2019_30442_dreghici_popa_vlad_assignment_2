package com.example.springdemo.services.activity;

import com.example.springdemo.dto.activity.ActivityDTO;
import com.example.springdemo.dto.activity.ActivityViewDTO;
import com.example.springdemo.dto.builders.activity.ActivityBuilder;
import com.example.springdemo.dto.builders.activity.ActivityViewBuilder;
import com.example.springdemo.dto.medication.DrugViewDTO;
import com.example.springdemo.entities.activity.Activity;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.activity.ActivityRepository;
import com.example.springdemo.validators.PersonFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ActivityService {

    @Autowired
    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public ActivityViewDTO findUserById(Integer id) {
        Optional<Activity> activity = activityRepository.findById(id);

        if (!activity.isPresent()) {
            throw new ResourceNotFoundException("Activity", "user id", id);
        }
        return ActivityViewBuilder.generateDTOFromEntity(activity.get());
    }

    public List<ActivityViewDTO> findAll() {
        List<Activity> activities = activityRepository.getAllOrdered();

        return activities.stream()
                .map(ActivityViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(ActivityDTO activityDTO) {

        PersonFieldValidator.validateInsertOrUpdateActivity(activityDTO);

        return activityRepository
                .save(ActivityBuilder.generateEntityFromDTO(activityDTO))
                .getId();
    }

    public Integer update(ActivityDTO activityDTO) {

        Optional<Activity> activity = activityRepository.findById(activityDTO.getId());

        if (!activity.isPresent()) {
            throw new ResourceNotFoundException("Person", "user id", activityDTO.getId().toString());
        }
        PersonFieldValidator.validateInsertOrUpdateActivity(activityDTO);

        return activityRepository.save(ActivityBuilder.generateEntityFromDTO(activityDTO)).getId();
    }

    public void delete(ActivityViewDTO activityViewDTO) {
        this.activityRepository.deleteById(activityViewDTO.getId());
    }
}
