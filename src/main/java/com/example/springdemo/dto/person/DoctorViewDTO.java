package com.example.springdemo.dto.person;

import com.example.springdemo.entities.enums.Role;
import com.example.springdemo.entities.person.Patient;

import java.util.Set;

public class DoctorViewDTO {
    private Integer id;
    private String name;
    private Role role;
    private String medicalRecord;

    public DoctorViewDTO(Integer id, String name, Role role, String medicalRecord) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.medicalRecord = medicalRecord;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }
}
