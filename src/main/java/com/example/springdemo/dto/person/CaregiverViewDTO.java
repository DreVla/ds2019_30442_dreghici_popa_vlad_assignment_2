package com.example.springdemo.dto.person;

import com.example.springdemo.entities.enums.Role;
import com.example.springdemo.entities.person.Patient;

import java.util.Set;

public class CaregiverViewDTO {

    private Integer id;
    private String name;
    private Role role;
    private String birthdate;
    private Set<Patient> patients;

    public CaregiverViewDTO(Integer id, String name, Role role,String birthdate, Set<Patient> patients) {
        this.id = id;
        this.name = name;
        this.role = role;
        this.birthdate = birthdate;
        this.patients = patients;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }
}
