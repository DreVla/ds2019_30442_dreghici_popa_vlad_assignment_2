package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.dto.person.DoctorViewDTO;
import com.example.springdemo.entities.person.Doctor;

public class DoctorViewBuilder {
    public static DoctorViewDTO generateDTOFromEntity(Doctor doctor) {
        return new DoctorViewDTO(
                doctor.getId(),
                doctor.getName(),
                doctor.getRole(),
                doctor.getMedicalRecord());
    }

    public static Doctor generateEntityFromDTO(DoctorViewDTO doctorViewDTO) {
        return new Doctor(
                doctorViewDTO.getId(),
                doctorViewDTO.getName(),
                doctorViewDTO.getMedicalRecord());
    }

    public static DoctorDTO generateDTOwithPass(Doctor doctor){
        return new DoctorDTO(
                doctor.getId(),
                doctor.getName(),
                doctor.getBirthdate(),
                doctor.getGender(),
                doctor.getRole(),
                doctor.getUsername(),
                doctor.getPassword(),
                doctor.getMedicalRecord());
    }
}
