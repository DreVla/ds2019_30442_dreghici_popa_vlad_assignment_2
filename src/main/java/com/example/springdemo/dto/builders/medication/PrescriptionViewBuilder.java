package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.DrugViewDTO;
import com.example.springdemo.dto.medication.PrescriptionViewDTO;
import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.medication.Prescription;

public class PrescriptionViewBuilder {

    public static PrescriptionViewDTO generateDTOFromEntity(Prescription prescription) {
        return new PrescriptionViewDTO(
                prescription.getId(),
                prescription.getDrug(),
                prescription.getIntake(),
                prescription.getMedicationPlan());

    }

    public static Prescription generateEntityFromDTO(PrescriptionViewDTO prescriptionViewDTO) {
        return new Prescription(
                prescriptionViewDTO.getId(),
                prescriptionViewDTO.getDrug(),
                prescriptionViewDTO.getIntake(),
                prescriptionViewDTO.getMedicationPlan());
    }
}
