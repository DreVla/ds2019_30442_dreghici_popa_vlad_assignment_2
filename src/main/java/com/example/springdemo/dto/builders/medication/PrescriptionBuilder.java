package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.dto.medication.PrescriptionDTO;
import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.medication.Prescription;

public class PrescriptionBuilder {

    public PrescriptionBuilder() {
    }

    public static PrescriptionDTO generateDTOFromEntity(Prescription prescription){
        return new PrescriptionDTO(
                prescription.getId(),
                prescription.getDrug(),
                prescription.getIntake(),
                prescription.getMedicationPlan());
    }

    public static Prescription generateEntityFromDTO(PrescriptionDTO prescriptionDTO) {
        return new Prescription(
                prescriptionDTO.getId(),
                prescriptionDTO.getDrug(),
                prescriptionDTO.getIntake(),
                prescriptionDTO.getMedicationPlan());
    }
}
