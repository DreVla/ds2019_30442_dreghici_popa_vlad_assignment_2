package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.dto.person.PersonDTO;
import com.example.springdemo.entities.person.Doctor;
import com.example.springdemo.entities.person.Person;

public class DoctorBuilder {

    public DoctorBuilder() {
    }

    public static DoctorDTO generateDTOFromEntity(Doctor doctor){
        return new DoctorDTO(
                doctor.getId(),
                doctor.getName(),
                doctor.getBirthdate(),
                doctor.getGender(),
                doctor.getRole(),
                doctor.getUsername(),
                doctor.getPassword(),
                doctor.getMedicalRecord());
    }

    public static Doctor generateEntityFromDTO(DoctorDTO doctorDTO){
        return new Doctor(
                doctorDTO.getId(),
                doctorDTO.getName(),
                doctorDTO.getBirthdate(),
                doctorDTO.getGender(),
                doctorDTO.getUsername(),
                doctorDTO.getPassword(),
                doctorDTO.getMedicalRecord());
    }
}
