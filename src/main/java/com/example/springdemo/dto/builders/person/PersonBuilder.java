package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.PersonDTO;
import com.example.springdemo.entities.person.Person;

public class PersonBuilder {

    public PersonBuilder() {
    }

    public static PersonDTO generateDTOFromEntity(Person person){
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getBirthdate(),
                person.getGender(),
                person.getRole(),
                person.getUsername(),
                person.getPassword());
    }

    public static Person generateEntityFromDTO(PersonDTO personDTO){
        return new Person(
                personDTO.getId(),
                personDTO.getName(),
                personDTO.getBirthdate(),
                personDTO.getGender(),
                personDTO.getRole(),
                personDTO.getUsername(),
                personDTO.getPassword());
    }
}
