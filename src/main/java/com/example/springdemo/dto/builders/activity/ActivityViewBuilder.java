package com.example.springdemo.dto.builders.activity;

import com.example.springdemo.dto.activity.ActivityViewDTO;
import com.example.springdemo.dto.medication.DrugViewDTO;
import com.example.springdemo.entities.activity.Activity;
import com.example.springdemo.entities.medication.Drug;

public class ActivityViewBuilder {

    public static ActivityViewDTO generateDTOFromEntity(Activity activity) {
        return new ActivityViewDTO(
                activity.getId(),
                activity.getStartDate(),
                activity.getEndDate(),
                activity.getActivity(),
                activity.getDuration());

    }

    public static Activity generateEntityFromDTO(ActivityViewDTO activityViewDTO) {
        return new Activity(
                activityViewDTO.getId(),
                activityViewDTO.getStartDate(),
                activityViewDTO.getEndDate(),
                activityViewDTO.getActivity(),
                activityViewDTO.getDuration());
    }
}
