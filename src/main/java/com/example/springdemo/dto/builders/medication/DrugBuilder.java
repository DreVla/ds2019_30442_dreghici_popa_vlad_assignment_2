package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.person.Patient;

public class DrugBuilder {

    public DrugBuilder() {
    }

    public static DrugDTO generateDTOFromEntity(Drug drug){
        return new DrugDTO(
                drug.getId(),
                drug.getName(),
                drug.getSideeffects(),
                drug.getPrescription());
    }

    public static Drug generateEntityFromDTO(DrugDTO drugDTO) {
        return new Drug(
                drugDTO.getId(),
                drugDTO.getName(),
                drugDTO.getSideeffects(),
                drugDTO.getPrescription());
    }
}
