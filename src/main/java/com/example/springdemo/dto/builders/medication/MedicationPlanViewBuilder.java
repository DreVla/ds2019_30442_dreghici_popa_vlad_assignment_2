package com.example.springdemo.dto.builders.medication;

import com.example.springdemo.dto.medication.MedicationPlanViewDTO;
import com.example.springdemo.dto.medication.PrescriptionViewDTO;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.entities.medication.Prescription;

public class MedicationPlanViewBuilder {
    public static MedicationPlanViewDTO generateDTOFromEntity(MedicationPlan medicationPlan) {
        return new MedicationPlanViewDTO(
                medicationPlan.getId(),
                medicationPlan.getPrescriptions(),
                medicationPlan.getPatient());

    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanViewDTO medicationPlanViewDTO) {
        return new MedicationPlan(
                medicationPlanViewDTO.getId(),
                medicationPlanViewDTO.getPrescriptionSet(),
                medicationPlanViewDTO.getPatient());

    }
}