package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.entities.person.Doctor;
import com.example.springdemo.entities.person.Patient;

public class PatientBuilder {

    public PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(
                patient.getId(),
                patient.getName(),
                patient.getBirthdate(),
                patient.getGender(),
                patient.getRole(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getCareGiver(),
                patient.getMedicationPlan());
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO) {
        return new Patient(
                patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getBirthdate(),
                patientDTO.getGender(),
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getCareGiver(),
                patientDTO.getMedicationPlan());
    }
}
