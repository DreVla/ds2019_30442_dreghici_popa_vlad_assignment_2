package com.example.springdemo.dto.builders.activity;

import com.example.springdemo.dto.activity.ActivityDTO;
import com.example.springdemo.dto.medication.DrugDTO;
import com.example.springdemo.entities.activity.Activity;
import com.example.springdemo.entities.medication.Drug;

public class ActivityBuilder {

    public ActivityBuilder() {
    }

    public static ActivityDTO generateDTOFromEntity(Activity activity){
        return new ActivityDTO(
                activity.getId(),
                activity.getStartDate(),
                activity.getEndDate(),
                activity.getActivity(),
                activity.getDuration());
    }

    public static Activity generateEntityFromDTO(ActivityDTO activityDTO) {
        return new Activity(
                activityDTO.getId(),
                activityDTO.getStartDate(),
                activityDTO.getEndDate(),
                activityDTO.getActivity(),
                activityDTO.getDuration());
    }
}
