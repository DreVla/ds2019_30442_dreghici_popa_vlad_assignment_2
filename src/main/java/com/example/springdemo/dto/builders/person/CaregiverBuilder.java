package com.example.springdemo.dto.builders.person;

import com.example.springdemo.dto.person.CaregiverDTO;
import com.example.springdemo.dto.person.DoctorDTO;
import com.example.springdemo.entities.person.Caregiver;
import com.example.springdemo.entities.person.Doctor;

public class CaregiverBuilder {

    public CaregiverBuilder() {
    }

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirthdate(),
                caregiver.getGender(),
                caregiver.getRole(),
                caregiver.getUsername(),
                caregiver.getPassword(),
                caregiver.getPatients());
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){
        return new Caregiver(
                caregiverDTO.getId(),
                caregiverDTO.getName(),
                caregiverDTO.getBirthdate(),
                caregiverDTO.getGender(),
                caregiverDTO.getUsername(),
                caregiverDTO.getPassword(),
                caregiverDTO.getPatientSet());
    }
}
