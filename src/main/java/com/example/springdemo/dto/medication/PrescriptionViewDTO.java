package com.example.springdemo.dto.medication;

import com.example.springdemo.entities.medication.Drug;
import com.example.springdemo.entities.medication.MedicationPlan;

public class PrescriptionViewDTO {
    private Integer id;
    private Drug drug;
    private String intake;
    private MedicationPlan medicationPlan;

    public PrescriptionViewDTO(Integer id, Drug drug, String intake, MedicationPlan medicationPlan) {
        this.id = id;
        this.drug = drug;
        this.intake = intake;
        this.medicationPlan = medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public String getIntake() {
        return intake;
    }

    public void setIntake(String intake) {
        this.intake = intake;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
