package com.example.springdemo.dto.medication;

import com.example.springdemo.entities.medication.Prescription;
import com.example.springdemo.entities.person.Patient;

import java.util.Objects;
import java.util.Set;

public class MedicationPlanDTO {

    private Integer id;
    private Set<Prescription> prescriptionSet;
    private Patient patient;

    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(Integer id, Set<Prescription> prescriptionSet, Patient patient) {
        this.id = id;
        this.prescriptionSet = prescriptionSet;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Prescription> getPrescriptionSet() {
        return prescriptionSet;
    }

    public void setPrescriptionSet(Set<Prescription> prescriptionSet) {
        this.prescriptionSet = prescriptionSet;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDTO that = (MedicationPlanDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(prescriptionSet, that.prescriptionSet) &&
                Objects.equals(patient, that.patient);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, prescriptionSet, patient);
    }
}
