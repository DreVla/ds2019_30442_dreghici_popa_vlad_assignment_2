package com.ds.quickmed.view.doctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.controller.DoctorController;
import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.utils.CaregiversRecyclerViewAdapter;
import com.ds.quickmed.view.doctor.fragments.add.AddCaregiverActivity;
import com.ds.quickmed.view.doctor.views.ViewCaregiverActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;


public class CaregiversFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Caregiver> caregivers = new ArrayList<>();
    private CaregiversRecyclerViewAdapter mAdapter;
    private DoctorController doctorController;
    private FloatingActionButton addCaregiver;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_caregivers, viewGroup, false);
        recyclerView = rootView.findViewById(R.id.caregivers_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new CaregiversRecyclerViewAdapter(getActivity(), caregivers);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        doctorController = new DoctorController();

        doctorController.getAllCaregivers(getActivity(), mAdapter);
        mAdapter.setClickListener(new CaregiversRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent caregiverPage = new Intent(getActivity(), ViewCaregiverActivity.class);
                Log.d(TAG, "onItemClick: " + mAdapter.getItem(position).getId());
                caregiverPage.putExtra("id", mAdapter.getItem(position).getId());
                caregiverPage.putExtra("obj",mAdapter.getItem(position));
                startActivity(caregiverPage);
            }

            @Override
            public void onRemoveClick(View view, int position) {
                doctorController.deleteCaregiver(getActivity(), mAdapter.getItem(position), mAdapter);
                doctorController.getAllCaregivers(getActivity(),mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });

        addCaregiver = rootView.findViewById(R.id.fab_add_caregiver);
        addCaregiver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddNewCaregiver();
            }
        });
        return rootView;
    }

    public void openAddNewCaregiver(){
        Intent intent = new Intent(getActivity(), AddCaregiverActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        doctorController.getAllCaregivers(getActivity(),mAdapter);

    }
}