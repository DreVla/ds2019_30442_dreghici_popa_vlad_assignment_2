package com.ds.quickmed.service;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CaregiverService {

//    private final static String API_URL = "http://192.168.43.136:8080/"; // hotspot
    private final static String API_URL = "http://10.0.2.2:8080/"; // emulator

    private static CaregiverApi caregiverApi;

    //get la instanta de retrofit
    public static CaregiverApi getInstance() {
        if (caregiverApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttp())
                    .build();

            caregiverApi = retrofit.create(CaregiverApi.class);
        }
        return caregiverApi;
    }

    private static OkHttpClient provideOkHttp() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        return httpBuilder.build();
    }


}
