package com.ds.quickmed.service;

import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.model.person.Patient;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CaregiverApi {

    @GET("/caregivers/{id}")
    Call<Caregiver> getCaregiverById(@Path("id") Integer var);

    @GET("/caregivers/username/{username}")
    Call<Caregiver> getCaregiverByUsername(@Path("username") String username);

    @GET("caregivers/{id}/all_patients")
    Call<List<Patient>> getAllPatientsForCaregiver(@Path("id") Integer var);

    @Headers({"Accept: application/json"})
    @POST("caregivers/insert/")
    Call<ResponseBody> addNewCaregiver(@Body Caregiver caregiver);

    @HTTP(method = "DELETE", path = "caregivers/delete", hasBody = true)
    Call<ResponseBody> deleteCaregiver(@Body Caregiver caregiver);

    @Headers({"Accept: application/json"})
    @PUT("caregivers/update/")
    Call<ResponseBody> updateCaregiver(@Body Caregiver caregiver);
}
