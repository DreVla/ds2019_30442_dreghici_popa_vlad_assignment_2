package com.ds.quickmed.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.model.medication.Drug;

import java.util.List;

public class DrugsRecyclerViewAdapter extends RecyclerView.Adapter<DrugsRecyclerViewAdapter.ViewHolder> {

    private List<Drug> mData;
    private LayoutInflater mInflater;
    private DrugsRecyclerViewAdapter.ItemClickListener mClickListener;

    // data is passed into the constructor
    public DrugsRecyclerViewAdapter(Context context, List<Drug> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public DrugsRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_view_row, parent, false);
        return new DrugsRecyclerViewAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(DrugsRecyclerViewAdapter.ViewHolder holder, int position) {
        String name = mData.get(position).getName();
        holder.myTextView.setText(name);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void replaceAll(List<Drug> toReplace){
        mData.clear();
        mData.addAll(toReplace);
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ImageView removeButton;

        ViewHolder(final View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.recycler_view_row_name_textview);
            removeButton = itemView.findViewById(R.id.remove_element_button);
            itemView.setOnClickListener(this);
            removeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.onRemoveClick(itemView, getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Drug getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(DrugsRecyclerViewAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
        void onRemoveClick(View view, int position);
    }
}

