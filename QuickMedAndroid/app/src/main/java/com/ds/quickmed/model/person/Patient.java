package com.ds.quickmed.model.person;

import com.ds.quickmed.model.enums.Gender;
import com.ds.quickmed.model.enums.Role;
import com.ds.quickmed.model.medication.MedicationPlan;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Patient extends Person implements Serializable {
    @SerializedName("caregiver")
    @Expose
    private Caregiver careGiver;
    @SerializedName("medicationPlan")
    @Expose
    private MedicationPlan medicationPlan;

    public Patient() {
    }

    public Patient(Integer id, String name,  Caregiver careGiver, MedicationPlan medicationPlan) {
        super(id, name, Role.PATIENT);
        this.careGiver = careGiver;
        this.medicationPlan = medicationPlan;
    }

    public Patient(Integer id, String name, String birthdate, Gender gender, String username, String password, Caregiver careGiver, MedicationPlan medicationPlan) {
        super(id, name, birthdate, gender, Role.PATIENT, username, password);
        this.careGiver = careGiver;
        this.medicationPlan = medicationPlan;
    }

    public Caregiver getCareGiver() {
        return careGiver;
    }

    public void setCareGiver(Caregiver careGiver) {
        this.careGiver = careGiver;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

}
