package com.ds.quickmed.utils;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.ds.quickmed.view.doctor.fragments.CaregiversFragment;
import com.ds.quickmed.view.doctor.fragments.DrugsFragment;
import com.ds.quickmed.view.doctor.fragments.PatientsFragment;

public class TabsAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;
    public TabsAdapter(FragmentManager fm, int NoofTabs){
        super(fm);
        this.mNumOfTabs = NoofTabs;
    }
    @Override
    public int getCount() {
        return mNumOfTabs;
    }
    @Override
    public Fragment getItem(int position){
        switch (position){
            case 0:
                CaregiversFragment caregiversFragment = new CaregiversFragment();
                return caregiversFragment;
            case 1:
                PatientsFragment patientsFragment = new PatientsFragment();
                return patientsFragment;
            case 2:
                DrugsFragment drugsFragment = new DrugsFragment();
                return drugsFragment;
            default:
                return null;
        }
    }
}