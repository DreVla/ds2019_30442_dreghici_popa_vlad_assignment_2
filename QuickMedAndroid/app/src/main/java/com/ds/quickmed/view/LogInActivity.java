package com.ds.quickmed.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ds.quickmed.R;
import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.model.person.Doctor;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.service.CaregiverService;
import com.ds.quickmed.service.DoctorService;
import com.ds.quickmed.service.PatientService;
import com.ds.quickmed.view.caregiver.MainCaregiverActivity;
import com.ds.quickmed.view.doctor.MainDoctorActivity;
import com.ds.quickmed.view.patient.view.MainPatientActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private EditText usernameTextField, passTextField;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        radioGroup = findViewById(R.id.radioGroup);

        usernameTextField = findViewById(R.id.login_username_edittext);
        passTextField = findViewById(R.id.login_password_edittext);

    }

    public void logIn(View view) {
        String usernameInput = usernameTextField.getText().toString();
        String passInput = passTextField.getText().toString();
        int id = radioGroup.getCheckedRadioButtonId();
        switch (id) {
            case R.id.radio_doctor:
                if(usernameInput != null && passInput != null)
                    logInAsDoctor(usernameInput, passInput);
                break;
            case R.id.radio_caregiver:
                if(usernameInput != null && passInput != null)
                    logInAsCaregiver(usernameInput, passInput);
                break;
            case R.id.radio_patient:
                if(usernameInput != null && passInput != null)
                    logInAsPatient(usernameInput, passInput);
                break;
            default:
                break;
        }

    }

    public void logInAsDoctor(final String username, final String pass){
        final Call<Doctor> call = DoctorService.getInstance().getDoctorByUsername(username);
        call.enqueue(new Callback<Doctor>() {
            @Override
            public void onResponse(Call<Doctor> call, Response<Doctor> response) {
                if(response.isSuccessful()){
                    Doctor doctor = response.body();
                    if(doctor.getPassword().equals(pass)){
                        Toast.makeText(LogInActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LogInActivity.this, MainDoctorActivity.class);
                        intent.putExtra("obj", doctor);
                        startActivity(intent);
                    } else {
                       //TODO insert dialog here
                        Toast.makeText(LogInActivity.this, "Pass wrong", Toast.LENGTH_SHORT).show();
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                        builder.setMessage(R.string.wrong_pass)
//                                .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        passTextField.setText("");
//                                    }
//                                });
                    }
                } else {
                   //TODO insert dialog here
                    Toast.makeText(LogInActivity.this, "User not found ", Toast.LENGTH_SHORT).show();
//                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                    builder.setMessage(R.string.not_found)
//                            .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    usernameTextField.setText("");
//                                    passTextField.setText("");
//                                }
//                            });
                }
            }

            @Override
            public void onFailure(Call<Doctor> call, Throwable t) {
                Toast.makeText(LogInActivity.this, "Wrong password or username!", Toast.LENGTH_SHORT).show();
                Log.d("FAIL", "onFailure: " + t.getCause());
//                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                builder.setMessage(R.string.smthwrong)
//                        .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                usernameTextField.setText("");
//                                passTextField.setText("");
//                            }
//                        });
            }
        });
    }

    public void logInAsCaregiver(final String username, final String pass){
        final Call<Caregiver> call = CaregiverService.getInstance().getCaregiverByUsername(username);
        call.enqueue(new Callback<Caregiver>() {
            @Override
            public void onResponse(Call<Caregiver> call, Response<Caregiver> response) {
                if(response.isSuccessful()){
                    Caregiver caregiver= response.body();
                    if(caregiver.getPassword().equals(pass)){
                        Toast.makeText(LogInActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LogInActivity.this, MainCaregiverActivity.class);
                        intent.putExtra("obj", caregiver);
                        startActivity(intent);
                    } else {
                        //TODO insert dialog here
                        Toast.makeText(LogInActivity.this, "Pass wrong", Toast.LENGTH_SHORT).show();
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                        builder.setMessage(R.string.wrong_pass)
//                                .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        passTextField.setText("");
//                                    }
//                                });
                    }
                } else {
                    //TODO insert dialog here
                    Toast.makeText(LogInActivity.this, "User not found", Toast.LENGTH_SHORT).show();
//                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                    builder.setMessage(R.string.not_found)
//                            .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    usernameTextField.setText("");
//                                    passTextField.setText("");
//                                }
//                            });
                }
            }

            @Override
            public void onFailure(Call<Caregiver> call, Throwable t) {
                Toast.makeText(LogInActivity.this, "Wrong password or username!", Toast.LENGTH_SHORT).show();
                Log.d("FAIL", "onFailure: " + t.getCause());
//                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                builder.setMessage(R.string.smthwrong)
//                        .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                usernameTextField.setText("");
//                                passTextField.setText("");
//                            }
//                        });
            }
        });
    }

    public void logInAsPatient(final String username, final String pass){
        final Call<Patient> call = PatientService.getInstance().getPatientByUsername(username);
        call.enqueue(new Callback<Patient>() {
            @Override
            public void onResponse(Call<Patient> call, Response<Patient> response) {
                if(response.isSuccessful()){
                    Patient patient= response.body();
                    if(patient.getPassword().equals(pass)){
                        Toast.makeText(LogInActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LogInActivity.this, MainPatientActivity.class);
                        intent.putExtra("obj", patient);
                        startActivity(intent);
                    } else {
                        //TODO insert dialog here
                        Toast.makeText(LogInActivity.this, "Pass wrong", Toast.LENGTH_SHORT).show();
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                        builder.setMessage(R.string.wrong_pass)
//                                .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        passTextField.setText("");
//                                    }
//                                });
                    }
                } else {
                    //TODO insert dialog here
                    Toast.makeText(LogInActivity.this, "User not found", Toast.LENGTH_SHORT).show();
//                    AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                    builder.setMessage(R.string.not_found)
//                            .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int id) {
//                                    usernameTextField.setText("");
//                                    passTextField.setText("");
//                                }
//                            });
                }
            }

            @Override
            public void onFailure(Call<Patient> call, Throwable t) {
                Toast.makeText(LogInActivity.this, "Wrong password or username!", Toast.LENGTH_SHORT).show();
                Log.d("FAIL", "onFailure: " + t.getCause());
//                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                builder.setMessage(R.string.smthwrong)
//                        .setPositiveButton(R.string.again, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                usernameTextField.setText("");
//                                passTextField.setText("");
//                            }
//                        });
            }
        });
    }
}
