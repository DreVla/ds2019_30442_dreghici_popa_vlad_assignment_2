package com.ds.quickmed.view.doctor.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.controller.DoctorController;
import com.ds.quickmed.model.medication.Drug;
import com.ds.quickmed.utils.DrugsRecyclerViewAdapter;
import com.ds.quickmed.view.doctor.fragments.add.AddDrugActivity;
import com.ds.quickmed.view.doctor.views.ViewDrugActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class DrugsFragment extends Fragment {
    private RecyclerView recyclerView;
    private DrugsRecyclerViewAdapter mAdapter;
    private List<Drug> drugs = new ArrayList<>();
    private DoctorController doctorController;
    private FloatingActionButton addDrug;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_drugs, viewGroup, false);
        recyclerView = rootView.findViewById(R.id.drugs_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new DrugsRecyclerViewAdapter(getActivity(), drugs);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        doctorController = new DoctorController();

        doctorController.getAllDrugs(getActivity(), mAdapter);
        mAdapter.setClickListener(new DrugsRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent drugIntent = new Intent(getActivity(), ViewDrugActivity.class);
                drugIntent.putExtra("id", mAdapter.getItem(position).getId());
                drugIntent.putExtra("obj", mAdapter.getItem(position));
                startActivity(drugIntent);
            }

            @Override
            public void onRemoveClick(View view, int position) {
                doctorController.deleteDrug(getActivity(), mAdapter.getItem(position), mAdapter);
                doctorController.getAllDrugs(getActivity(),mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        });
        addDrug = rootView.findViewById(R.id.fab_add_drug);
        addDrug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddNewDrug();
            }
        });
        return rootView;
    }

    public void openAddNewDrug(){
        Intent intent = new Intent(getActivity(), AddDrugActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        doctorController.getAllDrugs(getActivity(), mAdapter);
    }
}