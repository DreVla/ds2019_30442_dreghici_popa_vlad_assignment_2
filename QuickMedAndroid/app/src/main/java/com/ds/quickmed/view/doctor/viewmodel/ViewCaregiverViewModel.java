package com.ds.quickmed.view.doctor.viewmodel;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.ds.quickmed.model.person.Caregiver;
import com.ds.quickmed.service.CaregiverService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

public class ViewCaregiverViewModel extends ViewModel {

    public MutableLiveData<String> name = new MutableLiveData<>();
    public MutableLiveData<String> birthdate = new MutableLiveData<>();

    public void findById(int caregiverId){
        final Call<Caregiver> caregiverCall = CaregiverService.getInstance().getCaregiverById(caregiverId);

        caregiverCall.enqueue(new Callback<Caregiver>() {
            @Override
            public void onResponse(Call<Caregiver> call, Response<Caregiver> response) {
                if (response.isSuccessful()){
                    Log.d(TAG, "onResponse: " + response.body().getName());
                    Caregiver caregiver = response.body();
                    name.setValue(caregiver.getName());
                    birthdate.setValue(caregiver.getBirthdate());
                } else{
                    Log.d(TAG, "onResponse: Failure");
                }

            }

            @Override
            public void onFailure(Call<Caregiver> call, Throwable t) {
                Log.d(TAG, "onFailure: " +t.getCause());
            }
        });
    }

    public void sendPut(Caregiver caregiver, String name, String birthdate){
        caregiver.setName(name);
        caregiver.setBirthdate(birthdate);
        final Call<ResponseBody> caregiverCall = CaregiverService.getInstance().updateCaregiver(caregiver);
        caregiverCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    Log.d(TAG, "onResponse: Success");
                } else {
                    Log.d(TAG, "onResponse: Failure");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getCause());
            }
        });
    }

}
