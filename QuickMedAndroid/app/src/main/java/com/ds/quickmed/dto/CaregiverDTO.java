package com.ds.quickmed.dto;

import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.model.enums.*;
import java.util.Objects;
import java.util.Set;

public class CaregiverDTO {

    private Integer id;
    private String name;
    private String birthdate;
    private Gender gender;
    private Role role;
    private String username;
    private String password;
    private Set<Patient> patientSet;

    public CaregiverDTO() {
    }

    public CaregiverDTO(Integer id, String name, String birthdate, Gender gender, Role role, String username, String password, Set<Patient> patientSet) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.role = role;
        this.username = username;
        this.password = password;
        this.patientSet = patientSet;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Patient> getPatientSet() {
        return patientSet;
    }

    public void setPatientSet(Set<Patient> patientSet) {
        this.patientSet = patientSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDTO that = (CaregiverDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(birthdate, that.birthdate) &&
                gender == that.gender &&
                role == that.role &&
                Objects.equals(username, that.username) &&
                Objects.equals(password, that.password) &&
                Objects.equals(patientSet, that.patientSet);
    }
}

