package com.ds.quickmed.model.medication;


import com.ds.quickmed.model.person.Patient;

import java.io.Serializable;
import java.util.Set;

public class MedicationPlan implements Serializable {
    private Integer id;
    private Set<Prescription> prescriptions;
    private Patient patient;

    public MedicationPlan() {
    }

    public MedicationPlan(Integer id, Set<Prescription> prescription, Patient patient) {
        this.id = id;
        this.prescriptions = prescription;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<Prescription> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Set<Prescription> prescription) {
        this.prescriptions = prescription;
    }

    public void addPrescription(Prescription prescription){
        this.prescriptions.add(prescription);
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
