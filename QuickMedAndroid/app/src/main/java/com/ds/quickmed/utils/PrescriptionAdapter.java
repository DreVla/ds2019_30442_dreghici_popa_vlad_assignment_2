package com.ds.quickmed.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.model.medication.Prescription;

import java.util.List;

public class PrescriptionAdapter extends RecyclerView.Adapter<PrescriptionAdapter.ViewHolder> {

    private List<Prescription> mData;
    private LayoutInflater mInflater;
    private PrescriptionAdapter.ItemClickListener mClickListener;

    // data is passed into the constructor
    public PrescriptionAdapter(Context context, List<Prescription> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public PrescriptionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.prescription_row, parent, false);
        return new PrescriptionAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(PrescriptionAdapter.ViewHolder holder, int position) {
        String name = mData.get(position).getDrug().getName();
        String intake = mData.get(position).getIntake();
        holder.myTextView.setText(name);
        holder.intake.setText(intake);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void replaceAll(List<Prescription> toReplace){
        mData.clear();
        mData.addAll(toReplace);
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView intake;
        TextView myTextView;
        ViewHolder(final View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.prescription_drug_name_rv);
            intake = itemView.findViewById(R.id.prescription_intake_rv);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Prescription getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(PrescriptionAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

