package com.ds.quickmed.model.enums;

public enum Gender {
    MALE,
    FEMALE
}
