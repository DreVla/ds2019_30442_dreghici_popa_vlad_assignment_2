package com.ds.quickmed.service;

import com.ds.quickmed.model.medication.Drug;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DrugApi {

    @GET("/drugs/{id}")
    Call<Drug> getDrugById(@Path("id") Integer var);

    @Headers({"Accept: application/json"})
    @POST("drugs/insert/")
    Call<ResponseBody> addNewDrug(@Body Drug drug);

    @HTTP(method = "DELETE", path = "drugs/delete", hasBody = true)
    Call<ResponseBody> deleteDrug(@Body Drug drug);

    @Headers({"Accept: application/json"})
    @PUT("drugs/update/")
    Call<ResponseBody> updateDrug(@Body Drug drug);
}
