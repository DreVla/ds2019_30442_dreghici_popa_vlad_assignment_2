package com.ds.quickmed.view.patient.view;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.databinding.ActivityPatientMainBinding;
import com.ds.quickmed.model.medication.Prescription;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.service.DoctorService;
import com.ds.quickmed.utils.PrescriptionAdapter;
import com.ds.quickmed.view.patient.viewmodel.PatientMainViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPatientActivity extends AppCompatActivity {

    private PatientMainViewModel viewModel;
    private ActivityPatientMainBinding binding;
    private Patient patient;
    private RecyclerView recyclerView;
    private List<Prescription> prescriptions = new ArrayList<>();
    private PrescriptionAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_patient_main);
        viewModel = ViewModelProviders.of(this).get(PatientMainViewModel.class);

        binding.setLifecycleOwner(this);
        binding.setPatientViewModel(viewModel);

        recyclerView = findViewById(R.id.patient_view_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mAdapter = new PrescriptionAdapter(getApplicationContext(), prescriptions);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        Bundle b = getIntent().getExtras();
        patient = (Patient) b.getSerializable("obj");
        viewModel.findById(patient.getId());
        getAllPrescriptions();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_out:
                //TODO logout
//                logOut();
                break;
            case R.id.refresh:
                finish();
                startActivity(getIntent());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getAllPrescriptions(){
        final Call<List<Prescription>> call = DoctorService.getInstance().getAllPrescriptionsForMedicationPlan(patient.getMedicationPlan().getId());
        call.enqueue(new Callback<List<Prescription>>() {
            @Override
            public void onResponse(Call<List<Prescription>> call, Response<List<Prescription>> response) {
                if (response.isSuccessful()) {
                    Log.d("USERLIST", String.valueOf(response.body()));
                    List<Prescription> prescriptions = response.body();
                    if(prescriptions!=null){
                        mAdapter.replaceAll(prescriptions);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d("Response", "BAD");
                }
            }

            @Override
            public void onFailure(Call<List<Prescription>> call, Throwable t) {
            }
        });
    }
}
