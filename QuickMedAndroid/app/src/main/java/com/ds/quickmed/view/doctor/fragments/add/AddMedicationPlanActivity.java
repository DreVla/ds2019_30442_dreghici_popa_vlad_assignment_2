package com.ds.quickmed.view.doctor.fragments.add;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.model.medication.Prescription;
import com.ds.quickmed.model.person.Patient;
import com.ds.quickmed.service.DoctorService;
import com.ds.quickmed.utils.PrescriptionAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMedicationPlanActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Prescription> prescriptions = new ArrayList<>();
    private PrescriptionAdapter mAdapter;
    private Patient patient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medication_plan);
        recyclerView = findViewById(R.id.medicationplan_prescriptions_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mAdapter = new PrescriptionAdapter(getApplicationContext(), prescriptions);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        Bundle b = getIntent().getExtras();
        patient = (Patient) b.getSerializable("obj");
        getAllPrescriptions();
    }

    public void openAddPrescription(View view) {
        Intent intent = new Intent(getApplicationContext(), AddPrescriptionView.class);
        intent.putExtra("obj", patient);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getAllPrescriptions();

    }

    private void getAllPrescriptions(){
        final Call<List<Prescription>> call = DoctorService.getInstance().getAllPrescriptionsForMedicationPlan(patient.getMedicationPlan().getId());
        call.enqueue(new Callback<List<Prescription>>() {
            @Override
            public void onResponse(Call<List<Prescription>> call, Response<List<Prescription>> response) {
                if (response.isSuccessful()) {
                    Log.d("USERLIST", String.valueOf(response.body()));
                    List<Prescription> prescriptions = response.body();
                    if(prescriptions!=null){
                        mAdapter.replaceAll(prescriptions);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.d("Response", "BAD");
                }
            }

            @Override
            public void onFailure(Call<List<Prescription>> call, Throwable t) {
            }
        });
    }
}
