package com.ds.quickmed.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ds.quickmed.R;
import com.ds.quickmed.model.person.Patient;

import java.util.List;

public class CaregiverCurrentPatientsRVAdapter extends RecyclerView.Adapter<CaregiverCurrentPatientsRVAdapter.ViewHolder> {

    private List<Patient> mData;
    private LayoutInflater mInflater;
    private CaregiverCurrentPatientsRVAdapter.ItemClickListener mClickListener;

    // data is passed into the constructor
    public CaregiverCurrentPatientsRVAdapter(Context context, List<Patient> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public CaregiverCurrentPatientsRVAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.current_patients_row_item, parent, false);
        return new CaregiverCurrentPatientsRVAdapter.ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(CaregiverCurrentPatientsRVAdapter.ViewHolder holder, int position) {
        String name = mData.get(position).getName();
        holder.myTextView.setText(name);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void replaceAll(List<Patient> toReplace){
        mData.clear();
        mData.addAll(toReplace);
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myTextView;
        ViewHolder(final View itemView) {
            super(itemView);
            myTextView = itemView.findViewById(R.id.current_patients_item_name);


            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public Patient getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(CaregiverCurrentPatientsRVAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

