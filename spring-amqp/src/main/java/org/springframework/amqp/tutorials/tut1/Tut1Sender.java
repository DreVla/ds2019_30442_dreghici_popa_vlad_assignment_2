/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.amqp.tutorials.tut1;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.*;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * @author Gary Russell
 * @author Scott Deeg
 */
public class Tut1Sender {

	@Autowired
	private RabbitTemplate template;

	@Autowired
	private Queue queue;
	
	@Scheduled(fixedDelay = 1000, initialDelay = 500)
	public void send() {
		BufferedReader reader;
		try {
			Scanner scanner = new Scanner(new File("activities.txt"));
			while (scanner.hasNextLine()) {
//				System.out.println(scanner.nextLine());
				TimeUnit.SECONDS.sleep(1);
				System.out.println("Sending data");
				this.template.convertAndSend(queue.getName(), scanner.nextLine());
			}
			scanner.close();
		} catch (FileNotFoundException | InterruptedException e) {
			e.printStackTrace();
		}

	}



}
